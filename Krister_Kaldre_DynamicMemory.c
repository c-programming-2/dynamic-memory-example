#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SOURCEFILE         "loto300k.txt"
#define NAME_MAX           30
#define MAX_LOTO_NR       5
#define BUFLEN                  1024

typedef struct
{
    char eesnimi[NAME_MAX];
    char perekonnanimi[NAME_MAX];
    int     numbrid[MAX_LOTO_NR];
}t_isik;

typedef struct 
{
    char eesnimi[NAME_MAX];
    char perekonnanimi[NAME_MAX];
    int piletiArv;
}t_uusIsik;


typedef struct 
{
    int n,max;
    int m;
    t_isik *isikud;
    t_isik *tmp;
}t_array;



FILE* ava(const char *nimi,const char *tyyp)
{
    FILE *ptr = fopen(nimi,tyyp);
    
    if(ptr == NULL)
    {
        printf("Ei saanud avada '%s' faili\n",nimi);
        exit(0);
    }
    return ptr;
}
void loeAndmeid(t_array *arr,FILE*f)
{
    char buf[BUFLEN];
    int max = 100;//esialgne oletus
    arr->isikud = malloc(max *sizeof(*arr->isikud));
    if(arr->isikud == NULL)//malu eraldamine ebaonnestus
    {
        printf("Memory allocation Failed\n");
        return; //midagi ei saanud lugeda
    }
    arr->max = max; //kui eraldamine onnestus, siis muudame max vaartust
    while(1)
    {
        if(fgets(buf,BUFLEN,f) == NULL)
        {
            break;
        }
        if(sscanf(buf,"%s %s %d %d %d %d %d",arr->isikud[arr->n].eesnimi,arr->isikud[arr->n].perekonnanimi,&arr->isikud[arr->n].numbrid[0],
        &arr->isikud[arr->n].numbrid[1],&arr->isikud[arr->n].numbrid[2],&arr->isikud[arr->n].numbrid[3],&arr->isikud[arr->n].numbrid[4]) == 7)
        {
            arr->n++;//suurendame ainult siis kui real on sobilik info
        }
        if(arr->n == arr->max)
        {
            max = max*2;//suurendame mahtu 2 korda
            arr->tmp = realloc(arr->isikud,max*sizeof(*arr->isikud)); //kusime malu uue max suuruses
            if(arr->tmp == NULL)//kui malu eraldamine ebaonnestus
            {
                printf("New memory allocation failed\n");
                return;//midagi sai sisse loetud, sellega saame tegutseda
            }
            arr->isikud = arr->tmp;//kirjutame massiivi viide yle
            arr->max = max;
            
        }
        
    }
    
    
    
}
//sorteerin andmed nime jargi
int cmpfnc(const void *a, const void *b)
{
	t_isik *i1 = (t_isik *)a;
	t_isik *i2 = (t_isik *)b;
    
    int perenimiVordlus = strcmp(i1->perekonnanimi,i2->perekonnanimi);
	if(perenimiVordlus == 0)
    {
        //Kui perekonnanimi on sama, siis vordleme eesnime
        return strcmp(i1->eesnimi,i2->eesnimi);
    }
    else
    {
        return perenimiVordlus;
    }

}
//sorteeri andmed kahanevas jarjekorras
int numberVordlus(const void *a,const void *b )
{
    t_uusIsik *isik1 = (t_uusIsik*)a;
    t_uusIsik *isik2 = (t_uusIsik*)b;
    
    return isik2->piletiArv- isik1->piletiArv;
}

//otsi piletite arv
void otsi(t_array *a,t_uusIsik *isikuAndmed)
{
   isikuAndmed[a->m].piletiArv = 1;

   for(int i = 1; i < a->n;i++)
   {
       //andmed on sorteeritud, saame tegutseda yhe tsykliga
       if(strcmp(a->isikud[i].eesnimi,a->isikud[i - 1].eesnimi )== 0 &&
        strcmp(a->isikud[i].perekonnanimi,a->isikud[i - 1].perekonnanimi) == 0) //kontrollime kahte viided jarjest
        {
            isikuAndmed[a->m].piletiArv++; //suurendame, kui vajalik info on sobilik
        }
        else //Kui nimed on erinevad, kirjutame andmed t_uusisiku struktuuri
        {
            strcpy(isikuAndmed[a->m].eesnimi,a->isikud[i].eesnimi);
            strcpy(isikuAndmed[a->m].perekonnanimi,a->isikud[i].perekonnanimi);
            a->m++;
            isikuAndmed[a->m].piletiArv = 1;

        }
   }
   //printf("%d\n",a->m);//kontrolliks
    qsort(isikuAndmed,a->m ,sizeof(t_uusIsik),numberVordlus);//Sorteerin andmed kahanevas jarjekoras
    printf("Isikud, kes ostsid rohkem kui yhe pileti:\n");
    printf("eesnimi\t       perekonnanimi   piletite arv\n");
    for(int i = 0; i <= a->m;i++)
    {
           if(isikuAndmed[i].piletiArv > 1)
            {
                  printf("%-15s%-18s%d\n",isikuAndmed[i].eesnimi,isikuAndmed[i].perekonnanimi,isikuAndmed[i].piletiArv);
            }
    }
}
int main(void)
{
    FILE *f = ava(SOURCEFILE,"r");//lugemiseks
    t_array andmed = {0,0,0,NULL,NULL};//algvaartustan andmed 
    loeAndmeid(&andmed,f);//loen andmeid
    //printf("n/c    :    %d/%d\n",andmed.n,andmed.max);//Kontrolliks
    qsort(andmed.isikud,andmed.n,sizeof(t_isik),cmpfnc);//sorteerin andmed nime jargi
    t_uusIsik *isikuLoend = calloc(andmed.n ,sizeof(t_uusIsik));//allocating malu ja taidan maluruumi nullidega
    if(isikuLoend == NULL)//malu eraldamine ebaonnestus
    {
        printf("Memory could not be allocated\n");
        return 1;//ei saa otsida andmeid 
    }
    otsi(&andmed,isikuLoend);//otsin piletite arvud ja prindin valja
    fclose(f);//sulgen faili
    free(andmed.isikud);//vabastan malu
    free(isikuLoend);//vabastan malu

    return 0;
}
